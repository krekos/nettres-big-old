<?php
namespace Nettres\Models\Helpers;
use \Constants as CS;

class DatabaseManager extends \Nette\Object 
{
    
    public static $_conn;
    
    public static function  setConnecton($_conn)
    {
        self::$_conn = $_conn;
    }
    public static function countAll($_table)
    {
        return self::$_conn->table($_table)->count('*');
    }

}