<?php
namespace AclModule;

abstract class BasePresenter extends \BasePresenter
{
	
	function beforeRender()
	{
        $file = \Nette\Environment::expand(LANG_DIR . '/cs/admin.mo');
		$translator = new \Nette\Extras\GettextTranslator($file, 'cs');
        $this->template->setTranslator($translator);
    }	
	
	protected function _getTranslator($_file = 'admin', $_lang = 'cs')
	{
        $file = \Nette\Environment::expand(LANG_DIR . '/' . $_lang . '/' . $_file . '.mo');
        return new \Nette\Extras\GettextTranslator($file, $_lang);
    }
}