<?php

/**
 * My Application bootstrap file.
 */
use Nette\Application\Routers\Route;


// Load Nette Framework
require LIBS_DIR . '/Nette/loader.php';


// Configure application
$configurator = new Nette\Config\Configurator;

// Enable Nette Debugger for error visualisation & logging
//$configurator->setDebugMode($configurator::AUTO);
$configurator->enableDebugger(__DIR__ . '/../log');
$configurator->setDebugMode($configurator::PRODUCTION);

// Enable RobotLoader - this will load all classes automatically
$configurator->setTempDirectory(__DIR__ . '/../temp');
$configurator->createRobotLoader()
	->addDirectory(APP_DIR)
	->addDirectory(LIBS_DIR)
	->register();

// Create Dependency Injection container from config.neon file
$configurator->addConfig(__DIR__ . '/config/config.neon');
$container = $configurator->createContainer();

dibi::connect($container->parameters['dibi']);

// Setup router
$container->router[] = new Route('acl/<presenter>/<action>[/<id>]', array(
						'module' => 'Acl',
						'presenter' => 'Default',
						'action' => 'default'));
$container->router[] = new Route('admin/<presenter>/<action>[/<id>]', array(
						'module' => 'Administration',
						'presenter' => 'Sign',
						'action' => 'default'));

$container->router[] = new Route('<presenter>/[<action>[/id]]', array(
						'module' => 'Front',
						'presenter' => 'Default',
						'action' => 'default'));

$container->router[] = new Route('index.php', 'Homepage:default', Route::ONE_WAY);
$container->router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
// Configure and run the application!
$container->application->run();
