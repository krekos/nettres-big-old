<?php
namespace AdministrationModule;
use \Nette\Security as NS,
	\Nette\Database\Table\Selection;


/**
 * Users authenticator.
 *
 * @author     John Doe
 * @package    MyApplication
 */
class Authenticator extends \Nette\Object implements NS\IAuthenticator
{
	/** @var Nette\Database\Table\Selection */
	private $users;



	public function __construct(Selection $users)
	{
		$this->users = $users;
	}



	/**
	 * Performs an authentication
	 * @param  array
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;
		$row = $this->users->where('name', $username)->fetch();

		if (!$row) {
			throw new NS\AuthenticationException("Uživatel '$username' nebyl nalezen.", self::IDENTITY_NOT_FOUND);
		}

		if ($row->password !== $this->calculateHash($password)) {
			throw new NS\AuthenticationException("Špatné heslo!", self::INVALID_CREDENTIAL);
		}

		unset($row->password);

		$sql = \dibi::query('SELECT r.key_name
                                FROM [gui_acl_roles] AS r
                                RIGHT JOIN [gui_users_roles] AS us ON r.id=us.role_id
                                WHERE us.user_id=%i;', $row->id);
        $roles = $sql->fetchPairs();
		return new NS\Identity($row->id, $roles, $row->toArray());
	}



	/**
	 * Computes salted password hash.
	 * @param  string
	 * @return string
	 */
	public function calculateHash($password)
	{
		return hash('sha512', $password);
	}

}
