<?php
namespace AdministrationModule;
use \Nette\Database\Connection,
    \Nette\Database\Table\Selection;
/**
 * Description of Users
 *
 * @author Radim
 */
class Users extends Selection
{
	public function __construct(Connection $connection)
	{
		parent::__construct('gui_users', $connection);
	}
}

?>
