<?php
namespace AdministrationModule;
/**
 * Description of UsersPresenter
 *
 * @author Radim
 */
class UsersPresenter extends BasePresenter
{
	protected function startup()
	{
		parent::startup();
	}

	public function renderDefault()
	{
		
	}

	public function actionBrowseGroups()
	{
		
	}

	public function renderBrowseGroups()
	{
		
	}

	public function renderAllUsers()
	{
		$users = $this->context->createUsers();
		
		foreach ($users as $user)
		{
			$users_roles[$user -> id]['name'] = $user -> name;
            $sql2 = \dibi::query('SELECT r.id, r.name
                                    FROM [gui_acl_roles] AS r
                                    JOIN [gui_users_roles] AS u ON r.id=u.role_id
                                    WHERE u.user_id=%i
                                    ORDER BY r.name;',$user -> id);
            $roles = $sql2 -> fetchAll();
            $users_roles[$user -> id]['roles'] = array();
            foreach($roles as $role){
                $users_roles[$user -> id]['roles'][$role -> id] = $role -> name;
            }
        }
        $this->template->users = $users_roles;
	}

	public function actionUsersInCategory($id)
	{
		
	}

	public function renderUsersInCategory()
	{
		
	}

}