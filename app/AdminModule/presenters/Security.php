<?php
namespace AdministrationModule;

/**
 * Description of Security
 *
 * @author Radim
 */
class Security extends BasePresenter {

	/**
	 * (non-phpDoc)
	 *
	 * @see Nette\Application\Presenter#startup()
	 */
	protected function startup() {
		parent::startup();
		
		if (!$this->getUser()->isLoggedIn())
		{
			$this->flashMessage('You must be logged first!');
			$this->forward('Sign:default');
		}
	}

}