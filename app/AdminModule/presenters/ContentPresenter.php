<?php
namespace AdministrationModule;
/**
 * Description of ContentPresenter
 *
 * @author Radim
 */
class ContentPresenter extends Security {

	/**
	 * (non-phpDoc)
	 *
	 * @see Nette\Application\Presenter#startup()
	 */
	protected function startup() {
		parent::startup();
	}

	public function actionDefault() {
		
	}

	public function renderDefault() {
		
	}
	
	public function createComponentAddArticleForm()
	{
		
	}

	public function renderAddArticle() {
		
	}

	public function renderAddCategory() {
		
	}

	public function actionEditArticle() {
		
	}

}