<?php
namespace AdministrationModule;

use \Constants as CS;
/**
 * Description of HomePresenter
 *
 * @author Radim
 */
class HomePresenter extends Security {

	/**
	 * (non-phpDoc)
	 *
	 * @see Nette\Application\Presenter#startup()
	 */
	protected function startup() {
		parent::startup();
	}

	public function actionDefault() {
		
	}

	public function renderDefault()
	{
		$info = array('version' => CS::APP_VERSION, 'upgrade' => CS::LAST_UPGRADE, 'author' => CS::AUTHOR);
		
		$this->template->info = $info;
	}

}